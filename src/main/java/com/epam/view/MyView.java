package com.epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImpl;

import java.io.IOException;

public class MyView {
    private Controller controller;

    public MyView() throws IOException {
        controller = new ControllerImpl();
        controller.start();
    }
}
