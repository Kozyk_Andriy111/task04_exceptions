package com.epam.controller;

import com.epam.model.BusinessLogic;
import com.epam.model.Model;

import java.io.IOException;

public class ControllerImpl implements Controller{
    private Model model;

    @Override
    public void start() throws IOException {
        model = new BusinessLogic();
    }
}
