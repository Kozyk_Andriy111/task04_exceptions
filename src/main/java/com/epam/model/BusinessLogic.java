package com.epam.model;

import java.io.IOException;

public class BusinessLogic implements Model{
    private Car BMW_3;
    private Car BMW_5;
    private Car BMW_X7;
    private Car Audi_80;
    private Car Mercedes_E_class;
    private int exception = 1;

    public BusinessLogic() throws IOException {
        try {
            BMW_3 = new Car("BMW", "335i (E90)", 2010, "Grey", 20000, "CE 7264 KA");

            System.out.println("New auto added");
            Audi_80 = new Car("Audi", "80", 1992, "White", 5000, "CE 3546 ET");
            System.out.println("New auto added");
            BMW_X7 = new Car("BMW", "X7", 2019, "Black", 100000, "CE 5184 KO");
            System.out.println("New auto added");
            Mercedes_E_class = new Car("Mercedes", "E63 AMG (W212)", 2011, "Green", 50000, "CE 4823 CB");
            System.out.println("New auto added");
            if (exception == 1) {
                throw new IOException("There was an exception");
            }
            BMW_5 = new Car("BMW", "M5 (F10)", 2015, "Blue", 75000, "CE 2356 EC");
            System.out.println("New auto added");
        } catch (IOException e) {
            System.out.println("Don't worry, it is test");
        }
    }
}
