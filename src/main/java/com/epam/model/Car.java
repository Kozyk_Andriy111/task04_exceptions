package com.epam.model;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class Car implements AutoCloseable {
    private String brand;
    private String model;
    private int year;
    private String color;
    private int price;
    private String regNumber;
    private PrintWriter file;
    private int counterCar;

    public Car(String brand, String model, int year, String color, int price, String regNumber) {
        counterCar++;
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.color = color;
        this.price = price;
        this.regNumber = regNumber;
        System.out.println();
        System.out.println(toString());

        try {
            file = new PrintWriter(this.brand + " " + this.model + ".txt");
            file.println();
            file.println("Object brand: " + this.brand);
            file.println("Object model: " + this.model);
            file.println("Object creation number: " + this.counterCar);        } catch (FileNotFoundException e) {
            System.out.println("File not created");
        }
    }

    public void close() throws Exception {
        if (counterCar == 3) {
            throw new Exception("MyException in method close");
        }
        System.out.println("----------------------");
        System.out.println("File " + this.file + " was closed.");

        if (file != null) {
            System.out.println("Good. File != null.");
            file.close();
        }
    }

    @Override
    public String toString() {
        return "Brand: " + brand +
                "\nModel: " + model +
                "\nYear: " + year +
                "\nColor: " + color +
                "\nPrice: " + price +
                "\nRegistration Number: " + regNumber;
    }
}
